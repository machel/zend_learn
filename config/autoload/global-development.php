<?php
return [
    'zend_twig'       => [
        'environment'         => [
            'debug' => true,
            'cache' => false,
        ],
    ],
];
