<?php
namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170126172724 extends AbstractMigration
{
    public function getDescription()
    {
        return "Initial database schema";
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
            CREATE TABLE document (
               id int(11) NOT NULL auto_increment,
               content text NOT NULL,
               title varchar(100) NOT NULL,
               PRIMARY KEY (id)
            );
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("
            DROP TABLE document;
        ");
    }
}
