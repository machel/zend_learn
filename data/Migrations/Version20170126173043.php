<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170126173043 extends AbstractMigration
{
    public function getDescription()
    {
        return "Initial database data";
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
            INSERT INTO document (content, title)
                VALUES  ('Content 1',  'Title 1');
            INSERT INTO document (content, title)
                VALUES  ('Content 2',  'Title 2');
            INSERT INTO document (content, title)
                VALUES  ('Content 3',  'Title 3');
            INSERT INTO document (content, title)
                VALUES  ('Content 4',  'Title 4');
            INSERT INTO document (content, title)
                VALUES  ('Content 5',  'Title 5');
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("
            DELETE FROM document;
        ");
    }
}
