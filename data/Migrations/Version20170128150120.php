<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170128150120 extends AbstractMigration
{
    public function getDescription()
    {
        return "More database data";
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
            INSERT INTO document (content, title)
                VALUES  ('Content 101',  'Title 101');
            INSERT INTO document (content, title)
                VALUES  ('Content 102',  'Title 102');
            INSERT INTO document (content, title)
                VALUES  ('Content 103',  'Title 103');
            INSERT INTO document (content, title)
                VALUES  ('Content 104',  'Title 104');
            INSERT INTO document (content, title)
                VALUES  ('Content 105',  'Title 105');
            INSERT INTO document (content, title)
                VALUES  ('Content 106',  'Title 106');
            INSERT INTO document (content, title)
                VALUES  ('Content 107',  'Title 107');
            INSERT INTO document (content, title)
                VALUES  ('Content 108',  'Title 108');
            INSERT INTO document (content, title)
                VALUES  ('Content 109',  'Title 109');
            INSERT INTO document (content, title)
                VALUES  ('Content 110',  'Title 110');
            INSERT INTO document (content, title)
                VALUES  ('Content 111',  'Title 111');
            INSERT INTO document (content, title)
                VALUES  ('Content 112',  'Title 112');
            INSERT INTO document (content, title)
                VALUES  ('Content 113',  'Title 113');
            INSERT INTO document (content, title)
                VALUES  ('Content 114',  'Title 114');
            INSERT INTO document (content, title)
                VALUES  ('Content 115',  'Title 115');
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("
            DELETE FROM document WHERE content LIKE '%10%' || content LIKE '%11%';
        ");
    }
}
