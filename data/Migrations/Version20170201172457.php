<?php
namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170201172457 extends AbstractMigration
{
    public function getDescription()
    {
        return "create notes table and initial data";
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
            CREATE TABLE notes (
               id int(11) NOT NULL auto_increment,
               text text NOT NULL,
               title varchar(100) NOT NULL,
               PRIMARY KEY (id)
            );
        ");

        $this->addSql("
            INSERT INTO notes (text, title)
                VALUES  ('Text 1',  'Title 1');
            INSERT INTO notes (text, title)
                VALUES  ('Text 2',  'Title 2');
            INSERT INTO notes (text, title)
                VALUES  ('Text 3',  'Title 3');
            INSERT INTO notes (text, title)
                VALUES  ('Text 4',  'Title 4');
            INSERT INTO notes (text, title)
                VALUES  ('Text 5',  'Title 5');
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("
            DROP TABLE notes;
        ");
    }
}
