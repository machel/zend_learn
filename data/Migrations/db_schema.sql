CREATE TABLE document (
   id int(11) NOT NULL auto_increment,
   content text NOT NULL,
   title varchar(100) NOT NULL,
   PRIMARY KEY (id)
);

INSERT INTO document (content, title)
    VALUES  ('Content 1',  'Title 1');
INSERT INTO document (content, title)
    VALUES  ('Content 2',  'Title 2');
INSERT INTO document (content, title)
    VALUES  ('Content 3',  'Title 3');
INSERT INTO document (content, title)
    VALUES  ('Content 4',  'Title 4');
INSERT INTO document (content, title)
    VALUES  ('Content 5',  'Title 5');
