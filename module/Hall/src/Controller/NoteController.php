<?php
namespace Hall\Controller;

use Zend\View\Model\ViewModel;
use Hall\Model\NoteRepositoryInterface;

class NoteController extends \Zend\Mvc\Controller\AbstractActionController
{
    private $noteRepo = null;

    public function __construct(NoteRepositoryInterface $noteRepo)
    {
        $this->noteRepo = $noteRepo;
    }

    public function indexAction()
    {
        return new ViewModel([
            'notes' => $this->noteRepo->fetchAll(),
        ]);
    }

    public function detailAction()
    {
        $id = $this->params()->fromRoute('id');
        $note = $this->noteRepo->fetch($id);
        if (! $note) {
            return $this->redirect()->toRoute('note');
        }

        return new ViewModel([
            'note' => $note,
        ]);
    }
}
