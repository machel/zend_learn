<?php
namespace Hall\Factory;

use Hall\Controller\NoteController;
use Hall\Model\NoteRepositoryInterface;
use Interop\Container\ContainerInterface;

class NoteControllerFactory implements \Zend\ServiceManager\Factory\FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new NoteController($container->get(NoteRepositoryInterface::class));
    }
}
