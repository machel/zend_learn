<?php
namespace Hall\Model;

class Note
{
    private $id;        /// <int>
    private $title;     /// <string>
    private $text;      /// <string>

    public function __construct(string $title, string $text, int $id = null)
    {
        $this->title = $title;
        $this->text = $text;
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getText(): string
    {
        return $this->text;
    }
}
