<?php
namespace Hall\Model;

interface NoteCommandInterface
{
    public function insert(Note $note): Note;
    public function update(Note $note): Note;
    public function delete(Note $note): bool;
}
