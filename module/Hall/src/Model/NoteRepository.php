<?php
namespace Hall\Model;

class NoteRepository implements NoteRepositoryInterface
{
    public function fetchAll(): array
    {
        return array_map(function($note) {
            return new Note($note['title'], $note['text'], $note['id']);
        }, $this->data);
    }

    public function fetch(int $id): Note
    {
        if ( ! isset($this->data[$id])) {
            return null;
        }

        return new Note(
            $this->data[$id]['title'],
            $this->data[$id]['text'],
            $this->data[$id]['id']
        );
    }

    private $data = [
        1 => [
            'id'    => 1,
            'title' => 'Hello World #1',
            'text'  => 'This is our first note',
        ],
        2 => [
            'id'    => 2,
            'title' => 'Hello World #2',
            'text'  => 'This is our second note',
        ],
        3 => [
            'id'    => 3,
            'title' => 'Hello World #3',
            'text'  => 'This is our third note',
        ],
        4 => [
            'id'    => 4,
            'title' => 'Hello World #4',
            'text'  => 'This is our fourth note',
        ],
        5 => [
            'id'    => 5,
            'title' => 'Hello World #5',
            'text'  => 'This is our fifth note',
        ],
    ];
}
