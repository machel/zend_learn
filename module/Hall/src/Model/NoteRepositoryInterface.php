<?php
namespace Hall\Model;

interface NoteRepositoryInterface
{
    public function fetchAll();
    public function fetch(int $id): ?Note;
}
