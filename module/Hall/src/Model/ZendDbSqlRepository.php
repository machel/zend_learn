<?php
namespace Hall\Model;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Hydrator\HydratorInterface;
use Zend\Db\ResultSet\HydratingResultSet;

class ZendDbSqlRepository implements NoteRepositoryInterface
{
    private $db;
    private $hydrator;
    private $notePrototype;

    public function __construct(AdapterInterface $db, HydratorInterface $hydrator, Note $notePrototype)
    {
        $this->db = $db;
        $this->hydrator = $hydrator;
        $this->notePrototype = $notePrototype;
    }

    public function fetchAll()
    {
        $sql = new Sql($this->db);
        $select = $sql->select('notes');
        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        if ( ! $result instanceof ResultInterface || ! $result->isQueryResult()) {
            return [];
        }

        $resultSet = new HydratingResultSet($this->hydrator, $this->notePrototype);
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function fetch(int $id): ?Note
    {
        $sql = new Sql($this->db);
        $select = $sql->select('notes');
        $select->where(["id = ?" => $id]);
        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        if ( ! $result instanceof ResultInterface || ! $result->isQueryResult()) {
            return null;
        }

        $resultSet = new HydratingResultSet($this->hydrator, $this->notePrototype);
        $resultSet->initialize($result);

        $note = $resultSet->current();

        if (! $note) {
            return null;
        }

        return $note;
    }
}
