<?php
namespace Hall;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /*
    public function getServiceConfig()
    {
        return [
            'factories' => [
                Factory\ZendDbSqlRepositoryFactory::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    return new Factory\ZendDbSqlRepositoryFactory($dbAdapter);
                },
            ],
        ];
    }
     */
}
