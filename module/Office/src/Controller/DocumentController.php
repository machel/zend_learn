<?php
namespace Office\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Office\Model\Document;
use Office\Form\DocumentForm;

/**
 *  Documents management.
 */
class DocumentController extends AbstractActionController
{
    private $table;

    /**
     *  Constructor.
     */
    public function __construct(\Office\Model\DocumentTable $table)
    {
        $this->table = $table;
    }

    /**
     *  List documents.
     */
    public function indexAction()
    {
        $paginator = $this->table->fetchAll(true);  // paginator from DocumentTable
        $page = (int) $this->params()->fromQuery('page', 1);
        $page = ($page < 1) ? 1 : $page;
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(5);

        return new ViewModel(['paginator' => $paginator]);
    }

    /**
     *  Add document.
     */
    public function addAction()
    {
        // setup form
        $form = new DocumentForm();
        $form->get('submit')->setValue('Add');

        // get and check request
        $request = $this->getRequest();
        if ( ! $request->isPost()) {
            return ['form' => $form];
        }

        // validate data in form
        $document = new Document();
        $form->setInputFilter($document->getInputFilter());
        $form->setData($request->getPost());
        if ( ! $form->isValid()) {
            return ['form' => $form];
        }

        // save data
        $document->exchangeArray($form->getData());
        $this->table->save($document);

        return $this->redirect()->toRoute('document');
    }

    /**
     *  Edit document.
     */
    public function editAction()
    {
        // get document ID
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id === 0) {        // no id in HTTP params, add new document
            return $this->redirect()->toRoute('document', ['action' => 'add']);
        }

        // fetch document from storage
        try {
            $document = $this->table->fetch($id);
        } catch (\Exception $e) {   // ID does not exist, redirect to documents list
            return $this->redirect()->toRoute('document', ['action' => 'index']);
        }

        // setup form (with document data)
        $form = new DocumentForm();
        $form->bind($document);
        $form->get('submit')->setAttribute('value', 'Edit');

        // get request and form data
        $request = $this->getRequest();
        $viewData = ['id' => $id, 'form' => $form];

        if ( ! $request->isPost()) {
            return $viewData;
        }

        // validate data in form
        $form->setInputFilter($document->getInputFilter());
        $form->setData($request->getPost());
        if ( ! $form->isValid()) {
            return $viewData;
        }

        // save data
        $this->table->save($document);

        // Redirect to document list
        return $this->redirect()->toRoute('document', ['action' => 'index']);
    }

    /**
     *  Delete document.
     */
    public function deleteAction()
    {
        // get document ID
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id === 0) {        // no id in HTTP params, redirect to documents list
            return $this->redirect()->toRoute('document');
        }

        $request = $this->getRequest();

        // delete action posted (from confirmation page)?
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');
            if ($del === 'Yes') {
                $id = (int) $request->getPost('id');
                $this->table->delete($id);
            }

            return $this->redirect()->toRoute('document');
        }

        // show confirmation page (delete button clicked)
        return [
            'id' => $id,
            'document' => $this->table->fetch($id),
        ];
    }
}
