<?php
namespace Office\Controller;

use Interop\Container\ContainerInterface;

/**
 *  Factory.
 */
class DocumentControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new DocumentController($container->get('Office\Model\DocumentTable'));
    }
}
