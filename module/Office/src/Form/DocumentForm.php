<?php
namespace Office\Form;

use Zend\Form\Form;

/**
 *  Form.
 */
class DocumentForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('document');

        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);
        $this->add([
            'name' => 'title',
            'type' => 'text',
            'options' => [
                'label' => 'Title',
            ],
        ]);
        $this->add([
            'name' => 'content',
            'type' => 'text',
            'options' => [
                'label' => 'Content',
            ],
        ]);
        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Send',
                'id' => 'submitbutton',
            ],
        ]);

        $this->setAttribute('method', 'POST');
    }
}
