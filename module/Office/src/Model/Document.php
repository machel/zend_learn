<?php
namespace Office\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;

/**
 *  Document.
 */
class Document implements InputFilterAwareInterface
{
    public $id;             /// <int>
    public $content;        /// <string>
    public $title;          /// <string>

    private $inputFilter;   /// <InputFilterInterface>

    /**
     *  Array to object data.
     *  Required.
     */
    public function exchangeArray(array $data): void
    {
        $this->id     = (!empty($data['id'])) ? $data['id'] : null;
        $this->content = (!empty($data['content'])) ? $data['content'] : null;
        $this->title  = (!empty($data['title'])) ? $data['title'] : null;
    }

    /**
     *  Object to array.
     *  Required.
     */
    public function getArrayCopy(): array
    {
        return [
            'id'     => $this->id,
            'content' => $this->content,
            'title'  => $this->title,
        ];
    }

    /**
     *  Set data input filter.
     *  Switched off.
     */
    public function setInputFilter(InputFilterInterface $inputFilter): void
    {
        throw new \Exception(sprintf(
            '%s does not allow injection of an alternate input filter', __CLASS__
        ));
    }

    /**
     *  Get data input filter.
     */
    public function getInputFilter(): InputFilterInterface
    {
        if ($this->inputFilter) {
            return $this->inputFilter;
        }

        $inputFilter = new InputFilter();

        $inputFilter->add([
            'name' => 'id',
            'required' => true,
            'filters' => [
                ['name' => \Zend\Filter\ToInt::class]
            ],
        ]);

        $inputFilter->add([
            'name' => 'content',
            'required' => true,
            'filters' => [
                ['name' => \Zend\Filter\StripTags::class],
                ['name' => \Zend\Filter\StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 900,
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'title',
            'required' => true,
            'filters' => [
                ['name' => \Zend\Filter\StripTags::class],
                ['name' => \Zend\Filter\StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 100,
                    ],
                ],
            ],
        ]);

        $this->inputFilter = $inputFilter;
        return $this->inputFilter;
    }
}
