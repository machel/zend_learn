<?php
namespace Office\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/**
 *  Table.
 */
class DocumentTable
{
    protected $tableGateway = null;

    /**
     *  Constructor.
     */
    function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     *  Fetch all documents.
     */
    public function fetchAll($paginate = false)
    {
        if ($paginate) {
            return $this->fetchAllPaginated();
        } else {
            return $this->tableGateway->select();
        }
    }

    /**
     *  Fetch document by id.
     */
    public function fetch($id)
    {
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if ( ! $row) {
            throw new \Exception("Could not find row $id");
        }

        return $row;
    }

    /**
     *  Save given document.
     */
    public function save(Document $document)
    {
        $data = [
            'content' => $document->content,
            'title' => $document->title,
        ];
        if ($document->id == 0) {
            // insert new one
            $this->tableGateway->insert($data);
        } else {
            // update one
            if ($this->fetch($document->id)) {
                $this->tableGateway->update($data, ['id'=>$document->id]);
            } else {
                throw new \Exception("Document {$document->id} does not exist");
            }
        }
    }

    /**
     *  Delete document by id.
     */
    public function delete($id)
    {
        $this->tableGateway->delete(['id' => $id]);
    }

    /**
     *  fetchAll() Helper.
     */
    private function fetchAllPaginated()
    {
        // Create a new Select object for the table:
        $select = new Select($this->tableGateway->getTable());

        // Create a new result set based on the Document entity:
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Document());

        // Create a new pagination adapter object:
        $paginatorAdapter = new DbSelect(
            $select,                            // configured select object:
            $this->tableGateway->getAdapter(),  // the adapter to run it against:
            $resultSetPrototype                 // the result set to hydrate:
        );

        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
    }
}
