<?php
namespace Office;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    const VERSION = '3.0.2dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        // autoloading via composer (PSR-4)
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                Model\DocumentTable::class =>  function($container) {
                    $tableGateway = $container->get(Model\DocumentTableGateway::class);
                    return new Model\DocumentTable($tableGateway);
                },
                Model\DocumentTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Document());
                    return new TableGateway('document', $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [
                Controller\DocumentController::class => function($container) {
                    return new Controller\DocumentController(
                        $container->get(Model\DocumentTable::class)
                    );
                },
            ],
        ];
    }
}
