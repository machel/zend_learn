<?php
namespace OfficeTest\Controller;

use Office\Controller\DocumentController;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Office\Model\DocumentTable;
use Office\Model\Document;
use Zend\ServiceManager\ServiceManager;
use Prophecy\Argument;

/**
 *  Test DocumentController
 */
class DocumentControllerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;
    protected $documentTable;

    /**
     *  Setup tests.
     */
    public function setUp()
    {
        $configOverrides = []; // override configuration with test case specific values, like: sample view templates, path stacks, module_listener_options, etc.
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php', $configOverrides
        ));
        parent::setUp();
        $this->configureServiceManager($this->getApplicationServiceLocator());
    }

    /**
     *  Setup tests helper.
     */
    protected function configureServiceManager(ServiceManager $sm)
    {
        $sm->setAllowOverride(true);
        $sm->setService('config', $this->updateConfig($sm->get('config')));
        $sm->setService(DocumentTable::class, $this->mockDocumentTable()->reveal());
        $sm->setAllowOverride(false);
    }

    /**
     *  Setup tests helper.
     */
    protected function updateConfig($config)
    {
        $config['db'] = [];
        return $config;
    }

    /**
     *  Setup tests helper.
     */
    protected function mockDocumentTable()
    {
        $this->documentTable = $this->prophesize(DocumentTable::class);
        return $this->documentTable;
    }

    /**
     *  Test: controller accessibility.
     */
    public function testIndexActionCanBeAccessed()
    {
        $this->documentTable->fetchAll()->willReturn([]);
        $this->dispatch('/document');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Office');
        $this->assertControllerName(DocumentController::class);
        $this->assertControllerClass('DocumentController');
        $this->assertMatchedRouteName('document');
    }

    /**
     *  Test: add action redirection (after POST form data)
     */
    public function testAddActionRedirectsAfterValidPost()
    {
        $this->documentTable->save(Argument::type(Document::class))->shouldBeCalled();
        $postData = [
            'title' => 'Test Title A',
            'content' => 'Test Content A',
            'id' => '',
        ];
        $this->dispatch('/document/add', 'POST', $postData);
        $this->assertResponseStatusCode(302);
        $this->assertRedirectTo('/document');
    }
}
