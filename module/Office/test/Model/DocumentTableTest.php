<?php
namespace OfficeTest\Model;

use Office\Model\DocumentTable;
use Office\Model\Document;
use PHPUnit_Framework_TestCase as TestCase;
use Zend\Db\ResultSet\ResultSetInterface;
use Zend\Db\TableGateway\TableGatewayInterface;

class DocumentTableTest extends TestCase
{
    protected function setUp()
    {
        $this->tableGateway = $this->prophesize(TableGatewayInterface::class);
        $this->documentTable = new DocumentTable($this->tableGateway->reveal());
    }

    public function testFetchAllReturnsAllDocuments()
    {
        $resultSet = $this->prophesize(ResultSetInterface::class)->reveal();
        $this->tableGateway->select()->willReturn($resultSet);
        $this->assertSame($resultSet, $this->documentTable->fetchAll());
    }

    public function testCanDeleteAnDocumentByItsId()
    {
        $this->tableGateway->delete(['id' => 999])->shouldBeCalled();
        $this->documentTable->delete(999);
    }

    public function testSaveDocumentWillInsertNewDocumentsIfTheyDontAlreadyHaveAnId()
    {
        $data = [
            'content' => 'cont cont cont',
            'title'  => 'tit'
        ];
        $document = new Document();
        $document->exchangeArray($data);

        $this->tableGateway->insert($data)->shouldBeCalled();
        $this->documentTable->save($document);
    }

    public function testSaveDocumentWillUpdateExistingDocumentsIfTheyAlreadyHaveAnId()
    {
        $data = [
            'id'     => 999,
            'content' => 'cont cont cont',
            'title'  => 'tit',
        ];
        $document = new Document();
        $document->exchangeArray($data);

        $resultSet = $this->prophesize(ResultSetInterface::class);
        $resultSet->current()->willReturn($document);

        $this->tableGateway->select(['id' => 999])->willReturn($resultSet->reveal());
        $this->tableGateway
            ->update(
                array_filter($data, function ($key) {
                    return in_array($key, ['content', 'title']);
                }, ARRAY_FILTER_USE_KEY),
                ['id' => 999]
            )->shouldBeCalled();

        $this->documentTable->save($document);
    }

    public function testExceptionIsThrownWhenGettingNonExistentDocument()
    {
        $resultSet = $this->prophesize(ResultSetInterface::class);
        $resultSet->current()->willReturn(null);

        $this->tableGateway
            ->select(['id' => 999])
            ->willReturn($resultSet->reveal());

        $this->setExpectedException(
            \Exception::class,
            'Could not find row 999'
        );
        $this->documentTable->fetch(999);
    }
}
