<?php
namespace OfficeTest\Model;

use Office\Model\Document;
use PHPUnit_Framework_TestCase as TestCase;

class DocumentTest extends TestCase
{
    public function testInitialDocumentValuesAreNull()
    {
        $document = new Document();
        $this->assertNull($document->title, '"title" should be null');
        $this->assertNull($document->id, '"id" should be null');
        $this->assertNull($document->content, '"content" should be null');
    }

    public function testExchangeArraySetsPropertiesCorrectly()
    {
        $document = new Document();
        $data = [
            'title' => 'title ttt',
            'content' => 'content ccc',
            'id' => 999,
        ];
        $document->exchangeArray($data);
        $this->assertSame( $data['content'], $document->content, '"content" not set correctly');
        $this->assertSame( $data['id'], $document->id, '"id" not set correctly');
        $this->assertSame( $data['title'], $document->title, '"title" not set correctly');
    }

    public function testExchangeArraySetsPropertiesToNullIfKeysAreNotPresent()
    {
        $document = new Document();
        $data = [
            'title' => 'title ttt',
            'content' => 'content ccc',
            'id' => 999,
        ];
        $document->exchangeArray($data);
        $document->exchangeArray([]);
        $this->assertNull($document->title, '"title" should be null');
        $this->assertNull($document->id, '"id" should be null');
        $this->assertNull($document->content, '"content" should be null');
    }

    public function testGetArrayCopyReturnsAnArrayWithPropertyValues()
    {
        $document = new Document();
        $data = [
            'title' => 'title ttt',
            'content' => 'content ccc',
            'id' => 999,
        ];
        $document->exchangeArray($data);
        $copyArray = $document->getArrayCopy();
        $this->assertSame($data['title'], $copyArray['title'], '"title" not set correctly');
        $this->assertSame($data['id'], $copyArray['id'], '"id" not set correctly');
        $this->assertSame($data['content'], $copyArray['content'], '"content" not set correctly');
    }

    public function testInputFiltersAreSetCorrectly()
    {
        $document = new Document();
        $inputFilter = $document->getInputFilter();
        $this->assertSame(3, $inputFilter->count());
        $this->assertTrue($inputFilter->has('title'));
        $this->assertTrue($inputFilter->has('id'));
        $this->assertTrue($inputFilter->has('content'));
    }
}
